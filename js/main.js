jQuery(function ($) {

    // Carousel slider
    var $sliderRoot = $("#carousel");
    var currentIdx = -1;
    const slideSize = $('.slide-item', $sliderRoot).size() - 1;

    goToSlide(0);
    $('.dot', $sliderRoot).click(function () {
        goToSlide(parseInt($(this).attr('data-slide')));
    });

    $('.btn-prev', $sliderRoot).click(function () {
        const idx = currentIdx <= 0 ? slideSize : (currentIdx - 1);
        goToSlide(idx);
    });

    $('.btn-next', $sliderRoot).click(function () {
        const idx = currentIdx >= slideSize ? 0 : (currentIdx + 1);
        goToSlide(idx);
    });

    function goToSlide(slideIdx) {
        $('[data-slide=' + currentIdx + ']').removeClass('active');
        $('[data-slide=' + slideIdx + ']').addClass('active');

        $('.slide-item', $sliderRoot).css({display: 'none'});
        $($('.slide-item', $sliderRoot)[slideIdx]).css({display: 'block'});
        currentIdx = slideIdx;
    }


    /******************* Sung up - Login form *******************/

    var $loginForm = $("#login-form");
    var loginButton = $('button[type="submit"]', $loginForm);

    loginButton.prop('disabled', true);
    $loginForm.on('blur keyup change', 'input', checkLoginForm);

    function checkLoginForm() {
        var isValid = $loginForm.validate({
            validClass: "valid",
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    min: 5
                }
            }
        }).checkForm();

        loginButton.prop('disabled', !isValid)
    }


    /******************* Sung up - Register form ***********************/
    var $registerForm = $("#register-form");
    var registerButton = $('button[type="submit"]', $registerForm);

    registerButton.prop('disabled', true);
    $registerForm.on('blur keyup change', 'input', checkRegisterForm);

    function checkRegisterForm() {
        var isValid = $registerForm.validate({
            validClass: "valid",
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    min: 5
                },

                passMatch: {
                    equalTo: "#password"
                },

                checkboxApprove: {
                    required: true
                }

            },
            messages: {
                equalTo: "The password doesn't matched!"
            }
        }).checkForm();

        registerButton.prop('disabled', !isValid)
    }

    /******************* Article slilder  ***********************/

    var $articleSlider = $('#single-article');
    var sliderRef = $('#content-slider', $articleSlider).lightSlider({
        loop: true,
        keyPress: false,
        item: 4,
        slideEndAnimation: true,
        enabvarouch:false,
        enableDrag:false,
        responsive : [{
            breakpoint: 992,
            settings: {
                item: 2,
                enabvarouch: true,
                enableDrag: true,
            }
        }]
    });

    $('.btn-prev', $articleSlider).click(function () {
        sliderRef.goToPrevSlide();
    });
    $('.btn-next', $articleSlider).click(function () {
        sliderRef.goToNextSlide();
    });

    /******************* Site bar - mobile  ***********************/
    $('.toggleble', '.site-bar-mobile').find('.subnav-btn').click(toggleableMenu);
    $('.toggleble', '.filter-list-container').find('.subnav-btn').click(toggleableMenu);


    function toggleableMenu(ev) {
        var $parent = $(this).parent().hasClass('toggleble') ? $(this).parent() : $(this).parent().parent();

        if ($parent.hasClass('active')) {
            $parent.removeClass('active')
        } else {
            $('.toggleble').removeClass('active'); // TODO optimize the search!
            $parent.removeClass('active');
            $parent.addClass('active')
        }
    }



    /******************* Article-list  ***********************/
    var $articleList = $('.article-view-wrapper, .product-view-wrapper');
    if (localStorage.key('articleList')) {
        var mode = localStorage.getItem('articleList');
        $('.article-container', $articleList)
            .attr('class', 'article-container ' + (mode === 'list-view' ? 'flex-column' : null));
        $('.panel-list-view > i[data-view='+ mode +']', $articleList).addClass("active");
    } else{
        $('.panel-list-view > i[data-view="grid-view"]', $articleList).addClass("active");
    }

    $('.panel-list-view > i').click(function () {
        $('.panel-list-view > i').removeClass("active");
        $('.panel-list-view > i[data-view='+ $(this).attr('data-view') +']').addClass("active");
        if ($(this).attr('data-view') === 'list-view') {
            $('.article-container', $articleList).attr('class', 'article-container flex-column');
        } else {
            $('.article-container', $articleList).removeClass('flex-column');
        }

        localStorage.setItem('articleList', $(this).attr('data-view'));
    });

    /******************* resize filters and switch between mobile and desktop  ***********************/

    var $filterWrapper = $('.filter-wrapper');
    var $filterListContainer = $('.filter-list-container');
    var $pageSubNavigation = $('.page-sub-navigation');

    if ($(window).width() + 15 >= 992){
        $filterListContainer.removeClass('mobile');
        $filterListContainer.addClass('desktop');
        $filterWrapper.css('width', null);

        $pageSubNavigation.css('width', null);
        $pageSubNavigation.removeClass('mobile');
    } else {
        $filterListContainer.removeClass('desktop');
        $filterListContainer.addClass('mobile');
        $filterWrapper.css('width', $('.site-bar-mobile').width());

        $pageSubNavigation.css('width', $('.site-bar-mobile').width());
        $pageSubNavigation.addClass('mobile');
    }

    $(window).resize(function ( ev ) {
        if ($(this).width() + 15 <= 360) {
            return
        }
        if ($(this).width() + 15 >= 992) {
            $filterWrapper.attr('style', null);
            $pageSubNavigation.css('width', null);

            $pageSubNavigation.removeClass('mobile');

            $filterListContainer.removeClass('mobile');
            $filterListContainer.addClass('desktop');
           return;
       } else{
            $pageSubNavigation.addClass('mobile');

            $filterListContainer.removeClass('desktop');
            $filterListContainer.addClass('mobile');
        }

        $filterWrapper.css('width', $('.site-bar-mobile').width());
        $pageSubNavigation.css('width', $('.site-bar-mobile').width());

    });


    /******************* Tabs functionality  ***********************/


    var $tabContainer = $('#sing-up-container');

    $('a[data-id="sign-in"]', $tabContainer).addClass('active');
    $('.tab-content[id="sign-in"]', $tabContainer).addClass('active');

    $('.tab-navigation > a', $tabContainer).click(function () {
        $('a', $tabContainer).removeClass('active');
        $('.tab-content', $tabContainer).removeClass('active');

        $('.tab-content[id="'+$(this).attr('data-id')+'"]', $tabContainer).addClass('active');
        $(this).addClass('active');
    });
});
